package com.harambesa.settings; 
// Import required java libraries
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.RequestDispatcher;
import java.util.*;

import java.util.logging.Logger; 


// Implements Filter class
public class HarambesaFilter implements Filter  {
  Logger log = null;
  public void  init(FilterConfig config) 
  throws ServletException{ 
    log = Logger.getLogger(HarambesaFilter.class.getName());
  }
  public void  doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
  throws java.io.IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;    
    String path  = null;
    HttpSession session = req.getSession(false); 
    try{
      log.info("session duration"+session.getMaxInactiveInterval());

    }catch(NullPointerException e){
        log.info("session expired");
        log.info(e.getMessage());
    }
    String ref = req.getRequestURI();
    log.info(ref);
    log.info("===============");
    System.out.println(ref.matches("/mjet/$"));
      if(!ref.equals("mjet/request_handler") && 
        !ref.equals("mjet/re-login") && 
        !ref.matches("/mjet/$") &&
        !ref.matches(".*[css|jpg|png|gif|js]")){
        // ensure that a session exists
        if(session != null){
          String entity_id = (String)session.getAttribute("entity_id");
          // ensure user id exists in the session 
          log.info("the entity_id is "+entity_id);
          if(entity_id != null){
            chain.doFilter(request,response);
          }else{        
            log.info("User Not Logged In: Trying To Access: "+req.getRequestURI());
            log.info(req.getContextPath());
            path = req.getContextPath().toString()+"/login";
            // set tag to indicate action being done
            request.setAttribute("tag", "re_login"); 
            // set the page user was trying to access
            request.setAttribute("referer", req.getRequestURI()); 
            // 
            log.info("the referer is ="+(String)request.getAttribute("referer"));
            // res.sendRedirect(path);
            RequestDispatcher rd = request.getRequestDispatcher("/login/index.jsp"); 
            rd.forward(request,response);
          }
          // Pass request back down the filter chain
          
        }else{       
            log.info("User Not Logged In: Trying To Access: "+req.getRequestURI());
            log.info(req.getContextPath());
            path = req.getContextPath().toString()+"/login";
            // set tag to indicate action being done
            request.setAttribute("tag", "re_login"); 
            // set the page user was trying to access
            request.setAttribute("referer", req.getRequestURI()); 
            // 
            log.info("the referer is ="+(String)request.getAttribute("referer"));
            // res.sendRedirect(path);
            RequestDispatcher rd = request.getRequestDispatcher("/login/index.jsp"); 
            rd.forward(request,response);
        }
      }else{  
            log.info("accessing /login");
            chain.doFilter(request,response);
      }
      
   }
   public void destroy( ){
      /* Called before the Filter instance is removed 
      from service by the web container*/
   }
}
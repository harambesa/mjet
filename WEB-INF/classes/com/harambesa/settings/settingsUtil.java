package com.harambesa.settings;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;  

import com.harambesa.DBConnection.DBConnection; 

public class settingsUtil{ 
	/**
	*
	*	Check if entry exists in the database
	*
	*	@param connection 	the database connection object
	*	@param id 			the value to use to look up entry
	*	@param table		the table to lookup the entry in 
	*	@param column		the table column to lookup the value in
	*
	*
	*	@return ResultSet	Returns a resultset with cursor at the first item
	*/
	public static ResultSet entryExists(Connection conn, String value, String table, String column) 
	throws SQLException{
		ResultSet results=null;
		// sql string
		String sql = "select * from "+table+" where "+column+"= ? ";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1,value);
		// System.out.println(pstmt.toString());
		ResultSet rs = pstmt.executeQuery();
		if(rs.next()){ 
			results=rs;
		}else{
			rs = null;
			System.out.println("No record exists");
		}

		return results;
	}

}
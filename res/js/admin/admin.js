/**
*
*	Create a self invoking function to protect the global scope from populating with variables.
*	
*	Handles the settings page for email and password reset
*
*/
(function($){
	"use strict"
	// on document ready
	$(function(){
		// password reset
		$('#newUsers').click(function(e){
			e.preventDefault();
			$("#errorArea").html(" ");
			$(this).addClass('active').siblings('li').removeClass('active');
			newUsers();
		});
		// email notifications
		$('#activeUsers').click(function(e){
			e.preventDefault();
			$("#errorArea").html(" ");
			$(this).addClass('active').siblings('li').removeClass('active');
			// emailNotifications();
		});

		
	});

})(jQuery)


/**
*
*	Load new users in the system
*
*
*/
function newUsers(){
	$(".app_users").load("includes/newUsers.jsp", function(){
		console.log(this);
		$('#duration').change(function(){
			var duration = $("#duration option:selected").val();
			$.ajax({
				url: 	'/new-users',
				data: 	{'duration':duration},
				dataType: 	'json',
				type: 	'GET'
			})
			.done(function(result,responseText,xhr){
				console.log("success");
				console.log(result);
			})
			.fail(function(xhr){
				console.log("fail");
				console.log(xhr);
				errorDisplay(xhr,"An Error Occured, Contact your adminitor for assistance","danger");
				
			});
		});
	});
	/**
	*
	*
	*	Display the error message
	*
	*/
	function errorDisplay(xhr,message,bootstrap_class){
		$("#errorArea").html("<div class='alert alert-"+bootstrap_class+"'> "+message+"</div>");
	}
}